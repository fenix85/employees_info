from django.db import models


class Company(models.Model):
    name = models.CharField(
        max_length=255,
        unique=True
    )


class Department(models.Model):
    name = models.CharField(max_length=255)
    company = models.ForeignKey(
        Company,
        on_delete=models.CASCADE
    )

    class Meta:
        unique_together = ('name', 'company')


class Employee(models.Model):
    name = models.CharField(max_length=255)
    active = models.BooleanField(default=False)
    department = models.ForeignKey(
        Department,
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ('name',)
