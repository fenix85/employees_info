# Generated by Django 2.1.4 on 2019-01-01 14:37
import names
import random
from django.db import migrations

COMPANY_MODEL = "Company"
COMPANY_NAME = "Codecademy"
NUMBER = 11


def create_objects(apps, schema_editor):

    company_model = apps.get_model("departments", COMPANY_MODEL)
    company = company_model.objects.create(name=COMPANY_NAME)

    department_model = apps.get_model("departments", "Department")

    departments = [
        department_model.objects.create(
            name="Department_{number}".format(number=index),
            company=company
        ) for index in range(1, NUMBER)
    ]

    employee_model = apps.get_model("departments", "Employee")

    for department in departments:
        employee_model.objects.bulk_create([
            employee_model(
                name=names.get_full_name(),
                active=random.choice([True, False]),
                department=department
            ) for _ in range(1, NUMBER)
        ])


def rollback_migration(apps, schema_editor):
    company_model = apps.get_model("departments", COMPANY_MODEL)
    company_model.objects.get(name=COMPANY_NAME).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('departments', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(code=create_objects, reverse_code=rollback_migration)
    ]
