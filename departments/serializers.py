from rest_framework import serializers

from departments.models import Employee, Department, Company


class CompanySerializer(serializers.ModelSerializer):

    class Meta:
        model = Company
        fields = (
            'id', 'name'
        )


class BriefDepartmentSerializer(serializers.ModelSerializer):

    dpName = serializers.CharField(source='name')
    dpid = serializers.IntegerField(source='id')

    class Meta:
        model = Department
        fields = (
            'dpid', 'dpName'
        )


class FullDepartmentSerializer(serializers.ModelSerializer):

    dpName = serializers.CharField(source='name')
    dpid = serializers.IntegerField(source='id')
    company = CompanySerializer(read_only=True)

    class Meta:
        model = Department
        fields = (
            'dpid', 'dpName', 'company'
        )


class EmployeeSerializer(serializers.ModelSerializer):

    department = BriefDepartmentSerializer(read_only=True)

    class Meta:
        model = Employee
        fields = ('id', 'name', 'active', 'department')


class FullEmployeeSerializer(serializers.ModelSerializer):

    department = FullDepartmentSerializer(read_only=True)

    class Meta:
        model = Employee
        fields = ('id', 'name', 'active', 'department')



