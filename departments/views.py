from django_print_sql import print_sql, print_sql_decorator
from rest_framework.generics import ListAPIView, RetrieveUpdateDestroyAPIView
from django_filters import rest_framework as filters

from departments.filters import EmployeeSearchFilter
from departments.models import Employee
from departments.serializers import EmployeeSerializer, FullEmployeeSerializer


class ListEmployees(ListAPIView):

    serializer_class = EmployeeSerializer

    def get_queryset(self):
        company_id = self.kwargs.get('company_id')
        queryset = Employee.objects.select_related(
            'department__company'
        ).filter(
            department__company_id=company_id
        )
        return queryset

    @print_sql_decorator()
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)


class DetailEmployee(RetrieveUpdateDestroyAPIView):

    lookup_url_kwarg = 'employee_id'

    queryset = Employee.objects.all()

    serializer_class = FullEmployeeSerializer


class EmployeesSearchAPI(ListAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = EmployeeSearchFilter

