from django_filters import FilterSet, CharFilter

from departments.models import Employee


class EmployeeSearchFilter(FilterSet):

    name = CharFilter(lookup_expr='startswith')

    class Meta:
        model = Employee
        fields = ['name']
