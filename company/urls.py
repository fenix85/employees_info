"""company URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path

from departments.views import ListEmployees, DetailEmployee, EmployeesSearchAPI

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^employees/(?P<company_id>\d+)/$',
            ListEmployees.as_view(), name='api-employees'),
    re_path(r'^employee/(?P<employee_id>\d+)/$',
            DetailEmployee.as_view(), name='api-employee-details'),
    path('employee_search/',
            EmployeesSearchAPI.as_view(), name='api-employees-search'),


]
